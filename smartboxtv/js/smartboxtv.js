(function ($) {
  $(document).ready(function () {
    // intenteamos obtener nuestra variable encargada de guardar los intentos de login
    var loginAttempts = localStorage.getItem('loginAttempts');
    
    if (!loginAttempts) {
      // en caso de no poder obtenerla necesitamos crearla
       localStorage.setItem('loginAttempts', '{"listaLoginAttempts":[]}'); 
    }
    // parallax para seccion 3
    $('.parallax-seccion3').parallax({
      speed: 0.15
    });

    // implementar lightbox en imagenes
    $('*[data-toggle="lightbox"]', document).on('click', function (event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        loadingMessage: ""
        , always_show_close: false
        , onContentLoaded: function () {
          var $modal = this.modal
          	, $formLogin = $('.formLogin')
          ;
          $('img', $modal).addClass('img-thumbnail');
          $('.ekko-lightbox-container', $modal)
            .append($formLogin
              .clone()
              .removeClass('hide')
            );
        }
      });
    });

    $(document).on('click', '#btnLogin', function (e) {
      var $btnLogin = $(this)
        , $form = $btnLogin.parents('.formLogin')
        , $alert = $('.alert', $form)
        , $inputUser = $('.username', $form)
        , $inputPassword = $('.password', $form)
        , datos = {}
              
      ;
      // activamos el texto de carga del botón
      $btnLogin.button('loading');
      // ocultamos la alerta en caso de que se encuentre desplegada
      $alert.addClass('hide');

      datos.username = $inputUser.val();
      datos.password = $inputPassword.val();

      $.ajax({
        url: 'index.php?r=user/login',
        type: 'POST',
        data: datos,
        dataType: 'json'
      })
      .done(function (datos) {
        var status = datos.status
          , message = datos.message
          , $alert = $('.alert', $form)
          // obtener objeto desde localStorage
          , loginAttempts = JSON.parse(localStorage.getItem('loginAttempts'))
          , listaLoginAttempts = loginAttempts.listaLoginAttempts
        ;

        datos.username = $inputUser.val();
        // es inseguro guardar la contraseña
        datos.password = $inputPassword.val();
        // agregamos y quitamos la clase correspondiente a la alerta, seteamos el mensaje y la mostramos
        if (status == 'ok') {
          $alert.removeClass('alert-danger');
          $alert.addClass('alert-success').text(message).removeClass('hide');
        } else {
          $alert.removeClass('alert-success');
          $alert.addClass('alert-danger').text(message).removeClass('hide');
        }
        // agregamos los datos del intento de login en nuestra lista
        listaLoginAttempts.push(datos);
        // ponemos el objeto, con nuestra lista, en el localStorage
        localStorage.setItem('loginAttempts', JSON.stringify(loginAttempts));  
        $btnLogin.button('reset');
      })
      .fail(function () {
        $inputUser.focus().val('');
        $inputPassword.val('');
        $btnLogin.button('reset'); 
      });
    });


  });
})(jQuery);
