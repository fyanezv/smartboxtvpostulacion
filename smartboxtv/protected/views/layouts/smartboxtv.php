<?php /* @var $this Controller */ ?>

<!DOCTYPE html>
<html lang="es-CL">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'> 
    <link href='css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='css/ekko-lightbox.min.css' rel='stylesheet' type='text/css'>
    <link href="css/smartboxtv.css" rel='stylesheet' type="text/css">

    <script type="text/javascript" src="js/jquery-2.2.0.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script> 
    <script type="text/javascript" src="js/parallax-plugin.js"></script>
    <script type="text/javascript" src="js/ekko-lightbox.min.js"></script>    
    <script type="text/javascript" src="js/smartboxtv.js"></script>  
  </head>

  <?php echo $content; ?>
  
  </html>