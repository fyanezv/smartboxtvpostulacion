<body>
  <div id="principal">
    <div class="seccion1">
      <div class="container">
        <div class="row">
          <div class="col-lg-3"></div>
          <div class="col-lg-6">
            <img src="img/icon.png" class="" >
            <img src="img/NuncheeInteractive.png" class="">      
            <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc.</p>
          </div>
          <div class="col-lg-3"></div>           
        </div>
      </div>
    </div>
    <div class="seccion2">
      <div class="container">
        <div class="row">
          <h1>Nuestras Apps</h1>
          <div class="col-lg-4">
          <a href="img/kunga1.png" data-toggle="lightbox">
            <div class='wrapper-img'>
              <img src="img/kunga.png" class="img-responsive">  
              <div class='description-img'>  
                <p class='description_content'>KUNGA · Actitud Animal</p>
              </div>
            </div>
            </a>
          </div><!-- /.col-lg-4 -->
          <div class="col-lg-4">
            <a href="img/foto-middle.png" data-toggle="lightbox">
              <img src="img/foto-middle.png" class="img-circle img-responsive ">
            </a>
          </div><!-- /.col-lg-4 -->
          <div class="col-lg-4">
            <a href="img/directtv1.png" data-toggle="lightbox">
              <div class='wrapper-img'>
                <img src="img/directtv.png"  class="img-responsive">
                <div class='description-img'> 
                  <p class='description_content'>DirectTV · TV Interactive</p>
                </div>
              </div>
            </a>  
          </div><!-- /.col-lg-4 -->
        </div>
      </div>
    </div>
    <div class="seccion3 parallax-seccion3">  
    </div>
    <div class="seccion4">
      <img src="img/NuncheeInteractive White.png" class="img-responsive">
    </div>
    <form class="formLogin hide">
      <div class="form-group">
        <input type="text" class="form-control username"  placeholder="Username">
      </div>
      <div class="form-group">
        <input type="password" class="form-control password" placeholder="Password">
      </div>
      <a type="button" id="btnLogin" class="btn btn-primary btn-block" data-loading-text="Enviando ...">Enviar</a>
      <div id="alertLogin" class="alert" role="alert"><span></span></div>
    </form>
  </div>
</body>