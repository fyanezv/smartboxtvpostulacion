<?php

class UserController extends Controller {
  /*
   * Se encarga de recibir los parámetros indicados por el usuario.
   * conectarse al servicio y  por medio del http status code entregar una respuesta al usuario 
   */
  public function actionLogin() {
    $username = Yii::app()->request->getPost('username');
    $password = Yii::app()->request->getPost('password');

    $url = trim("http://54.175.140.63/nunchee/api/1.0/users/login_frontend");
   
    
    // definimos parámetros
    $parametros_post = 'username=' . urlencode($username) . '&password=' . urlencode($password);
    // iniciamos
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6");
    // definir tipo de petición a realizar: POST
    curl_setopt($ch, CURLOPT_POST, 1);
    // Le pasamos los parámetros definidos anteriormente
    curl_setopt($ch, CURLOPT_POSTFIELDS, $parametros_post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // ejecutamos la petición
    $response = curl_exec($ch);

    if (curl_error($ch)) {
      $error = curl_error($ch);
    }
    // obtenemos el código http que es el que nos intereza para dar formato al mensaje
    // que el usuario recibirá
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    //HARDCODE PARA PRUEBAS
    //$httpcode = 404;
    
    // cerramos la conexión
    curl_close($ch);

    $this->enviarRespuesta($httpcode);
  }

  public function enviarRespuesta ($httpcode){
    
    switch ($httpcode) {
      case 200:
        $respuesta = array('status' => 'ok', 'message' => 'Inicio de sesión correcto');
        break;

      case 403:
        $respuesta = array('status' => 'error', 'message' => 'No autorizado');
        break;
      
      case 404:
        $respuesta = array('status' => 'error', 'message' => 'Problemas al realizar el inicio de sesión');
        break;
      
      case 501:
        $respuesta = array('status' => 'error', 'message' => 'Problemas internos del servidor');
        break;
      
      case 502:
        $respuesta = array('status' => 'error', 'message' => 'Colapso del servidor');
        break;
      // agregué esta opción que es la utilizada cuando no es posible conectarse al servicio
      default:
         $respuesta = array('status' => 'error', 'message' => 'No se pudo conectar al servidor');
        break;
      
    }
    // agregamos fecha y hora a la respuesta
    $respuesta['fecha'] = date('d-m-Y');
    $respuesta['hora'] = date('H:i:s');
    // enviamos respuesta
    $this->renderJSON($respuesta);
  }
  
  protected function renderJSON($respuesta) {
    header('Content-type: application/json');
    echo CJSON::encode($respuesta);

    foreach (Yii::app()->log->routes as $route) {
      if ($route instanceof CWebLogRoute) {
        $route->enabled = false; // disable any weblogroutes
      }
    }
    Yii::app()->end();
  }
  
}
