<?php

class SiteController extends Controller {

  /**
   * This is the default 'index' action that is invoked
   * when an action is not explicitly requested by users.
   */
  public function actionIndex() {

    // utilizar nuestro layout y nuestra vista
    $this->layout = "/layouts/smartboxtv";
    $this->render('/smartboxtv/index'); 
  }

}
